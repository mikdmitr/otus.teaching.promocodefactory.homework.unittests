using System.Collections.Generic;
using Castle.Core.Internal;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class MockRepositoryBuilder<T> where T : BaseEntity
    {
        public MockRepositoryBuilder<T> SetData(List<T> tList)
        {
            _dataList = tList;
            return this;
        }

        public MockRepositoryBuilder<T> EnableGetByIdAsync()
        {
            _isEnableGetByIdAsync = true;
            return this;
        }

        public MockRepositoryBuilder<T> EnableUpdateAsync()
        {
            _isEnableUpdateAsync = true;
            return this;
        }

        public Mock<IRepository<T>> Build()
        {
            _repositoryMock = new Mock<IRepository<T>>();
            
            if (_isEnableGetByIdAsync && !_dataList.IsNullOrEmpty())
            {
                foreach (var data in _dataList)
                {
                    _repositoryMock.Setup(m => m.GetByIdAsync(data.Id)).ReturnsAsync(data);
                }
            }

            if (_isEnableUpdateAsync)
            {
                _repositoryMock.Setup(m => m.UpdateAsync(It.IsAny<T>()));
            }

            return _repositoryMock;
        }
        
        private Mock<IRepository<T>> _repositoryMock;
        private List<T> _dataList;
        private bool _isEnableGetByIdAsync;
        private bool _isEnableUpdateAsync;
    }
}